extern crate ncurses;

use ncurses::*;

use std::time::{Duration, Instant};

use rand::Rng;

fn main() {
    //let mut grid1:[[bool;512];512] = [[false;512];512]; // FIXME: don't know how to make this dynamic sized
    initscr(); // initialse curses
     /* Get the screen bounds. */
    let mut max_x:i32 = 0;
    let mut max_y:i32 = 0;
    
    timeout(10);
    getmaxyx(stdscr(), &mut max_y, &mut max_x);
    
    let mut grid1 = vec![vec![false; max_y as usize]; max_x as usize];

    let mut run:bool = false;
    let glines:i32 = max_y - 2; // usable grid minus borders
    let gcols:i32 = max_x - 2;  // usable grid minus borders
    border(ACS_VLINE(), ACS_VLINE(), ACS_HLINE(), ACS_HLINE(), ACS_ULCORNER(), ACS_URCORNER(), ACS_LLCORNER(), ACS_LRCORNER());

    rand_field ( &mut grid1 , glines, gcols ); // starting point is random
    draw_field ( &grid1, glines, gcols );  // draw the grid

    let xystr = format!("  x {} y {}   ", max_x, max_y ); 
    mvaddstr(5, 5, &xystr);

    refresh(); // tell curses to refresh the screen

    loop {
        if run { // if in run mode, calculate and redraw
            calc_field ( &mut grid1 , glines, gcols );
            draw_field ( &grid1, glines, gcols );
        }
        refresh(); // tell curses to draw the screen
        let k=getch(); // read a key into var 'k' from keyboard
        match k {  // FIXME:  how to match against a key like 'x' instead of 120?
            32  => { rand_field ( &mut grid1, glines, gcols ); } //restart
            45  => { // -  -- draw a single line of three
                clear_field ( &mut grid1, glines, gcols ); // clear grid
                dash_field ( &mut grid1 );  // just populate a *** pattern
                draw_field  ( &grid1, glines, gcols ); // draw it
            }
            114 => {run=!run;} // r -- stop and start
            115 => { // s  -- single step
                calc_field ( &mut grid1, glines, gcols );
                draw_field ( &grid1, glines, gcols );
            }
            116 => { // t -- test 1,000,000 calc loops
                endwin();
                let start = Instant::now();
                for _ in 0..100000 {
                    calc_field ( &mut grid1, glines, gcols );
                }
                let duration = start.elapsed();
                println!("Time elapsed in original calc is: {:?}", duration);
                break;
            }
            120 => { break ; } // x - exit
            _  => { }
        }
    }
    endwin(); // terminate curses
}

// draw the field from grid
fn draw_field ( agrid:  &Vec<Vec<bool>> , glines:i32, gcols:i32 ) {
    let star:chtype = '*' as chtype;
    let space:chtype = ' ' as chtype;
    for x in 0..gcols {
        for y in 0..glines {
            if agrid[x as usize][y as usize] {
                mvaddch(y+1,x+1,star);
            }
            else {
                mvaddch(y+1,x+1,space);
            }
        }
    }
}

// populate the grid with just a single line for testing
fn dash_field ( agrid: &mut Vec<Vec<bool>>  ) {
    agrid[4][4]=true;
    agrid[4][5]=true;
    agrid[4][6]=true;
}

// clear the working grid
fn clear_field ( agrid: &mut Vec<Vec<bool>> , glines:i32, gcols:i32 ) {
    for x in 0..=gcols{
        for y in 0..=glines{
            agrid[x as usize][y as usize] = false;
        }
    }
}

// set the working grid to random
fn rand_field( agrid: &mut Vec<Vec<bool>> , glines:i32, gcols:i32 ) {

	let mut rng = rand::thread_rng();
	
    // our random number is just 0 or 1, for false or true
	for x in 0..=gcols{
        for y in 0..=glines{
	        let num: u8 = rng.gen_range(0..=1);
            if num == 0 {
                agrid[x as usize][y as usize] = false
            } else {
                agrid[x as usize][y as usize] = true
            }
        }
	}
}
// set the working grid to random
fn rand_field_new( agrid: &mut Vec<Vec<bool>> , glines:i32, gcols:i32 ) {

	let mut rng = rand::thread_rng();
	
    // our random number is just 0 or 1, for false or true
	for x in 0..=gcols{
        for y in 0..=glines{
	        let num: u8 = rng.gen_range(0..=1);
            if num == 0 {
                agrid[x as usize][y as usize] = false
            } else {
                agrid[x as usize][y as usize] = true
            }
        }
	}
}

// main Life algorithm,  build a new grid 'calc' based on the old grid
// and life and death rules uses static array
fn calc_field ( agrid: &mut Vec<Vec<bool>> , glines:i32, gcols:i32) {
    let mx:usize = gcols as usize +2;
    let my:usize = glines as usize+2;
    let mut calc = vec![vec![false; my ]; mx];
    for line in 0..glines {
        for col in 0..gcols {
            let mut n: i8 = 0;
            // variables for surrounding co-ordinates
            // allows easy wrap-around edge code
            let mut upper = line - 1;
            if upper < 0 { upper = glines - 1 ; }
            let mut lower = line + 1;
            if lower == glines { lower = 0 ; }
            let mut left = col - 1;
            if left < 0 { left = gcols - 1 ; }
            let mut right = col + 1;
            if right == gcols { right = 0 ; }
            let ucol:usize = col as usize;
            let uline:usize = line as usize;
            let uleft:usize = left as usize;
            let uright:usize = right as usize;
            let uupper:usize = upper as usize;
            let ulower:usize = lower as usize;
            // count the living neighbors
            if agrid[uleft][uupper] { n+=1 }
			if agrid[uright][uupper] { n+=1 }
			if agrid[ucol][uupper] { n+=1 }
			if agrid[uleft][uline] { n+=1 }
			if agrid[uright][uline] { n+=1 }
			if agrid[uleft][ulower] { n+=1 }
			if agrid[uright][ulower] { n+=1 }
			if agrid[ucol][ulower] { n+=1 }
            // if alive, then stay alive if 2 or 3 neigbors, else die
            if agrid[ucol][uline] {
                if n == 2 || n == 3 { 
                    calc[ucol][uline] = true; 
                } else {
                    calc[ucol][uline] = false;
                }
            } else {
                // if not alive, and 3 neighbors, then born
                if n == 3 {
                    calc[ucol][uline] = true; 
                }
            }
            
        }
    }
    // copy new state back to original grid
    *agrid = calc;
}
