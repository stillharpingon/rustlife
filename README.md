# rustlife



## How to use

Run the simulation with the 'life' binary.

Initial state is random.

## controls

Key | Effect | Comment
---|---|---
r | run / stop | starts and stops the simulation
space | randomise | replaces the current state with random
dash | line | places only a single line for testing
x | exit | quits

Enjoy